<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

class MaintenanceScheduleAddTest extends Base
{
    protected $classname = "MaintenanceScheduleAdd";

    public function testRendering()
    {
        $data = [
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '0 23 * * *',
            'duration'         => 90,
            'leadTime'         => 30,
            'area'             => 'zms',
            'announcement'     => 'Wait for it',
            'documentBody'     => 'There it is',
            'save'             => 'save',
        ];

        $this->setApiCalls([
            [
                'function' => 'readGetResult',
                'url' => '/workstation/',
                'parameters' => ['resolveReferences' => 1],
                'response' => $this->readFixture("GET_Workstation_Resolved1.json")
            ],
            [
                'function' => 'readPostResult',
                'url' => '/maintenanceschedule/',
                'response' => $this->readFixture("GET_maintenanceschedule_1000000000.json")
            ],
        ]);

        $response = $this->render([], $data, [], 'POST');

        $this->assertRedirect($response, '/maintenance/?success=maintenance_schedule_add');
        $this->assertEquals(302, $response->getStatusCode());
    }
}
