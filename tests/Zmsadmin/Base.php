<?php

/**
 *
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 *
 */
namespace BO\Zmsadmin\Tests;

use BO\Slim\Response;

abstract class Base extends \BO\Zmsclient\PhpUnit\Base
{
    protected $namespace = '\\BO\\Zmsadmin\\';

    public function readFixture($filename)
    {
        $path = dirname(__FILE__) . '/fixtures/' . $filename;
        if (! is_readable($path) || ! is_file($path)) {
            throw new \Exception("Fixture $path is not readable");
        }
        return file_get_contents($path);
    }

    protected function getApiCalls()
    {
        return array_merge($this->getOptionalApiCalls(), parent::getApiCalls());
    }

    protected function getOptionalApiCalls(): array
    {
        $exception = new \BO\Zmsclient\Exception('API-Call failed', new Response(404));
        $exception->template = 'bo/zmsclient/exception/apifailed';

        return [
            'GET_maintenanceschedule_active' => [
                'function'   => 'readGetResult',
                'url'        => '/maintenanceschedule/active/',
                'exception'  => $exception,
                'optional'   => true,
            ],
            'GET_config' => [
                'function'   => 'readGetResult',
                'url'        => '/config/',
                'response' => $this->readFixture("GET_config.json"),
                'optional'   => true,
            ],
        ];
    }
}
