<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsadmin\Tests;

class MaintenanceListTest extends Base
{
    protected $classname = "MaintenanceList";

    public function testRendering()
    {
        $this->setApiCalls(
            [
                [
                    'function' => 'readGetResult',
                    'url' => '/workstation/',
                    'parameters' => ['resolveReferences' => 0],
                    'response' => $this->readFixture("GET_Workstation_Resolved1.json")
                ],
                [
                    'function' => 'readGetResult',
                    'url' => '/maintenanceschedule/',
                    'response' => $this->readFixture("GET_maintenanceschedule_list.json")
                ],
            ]
        );

        $response = $this->render([], []);

        self::assertStringContainsString('23 20 * * *', (string) $response->getBody());
        self::assertStringContainsString('2016-04-01 15:15:00', (string) $response->getBody());
    }
}
