<?php
/**
 * @package Zmsadmin
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

namespace BO\Zmsadmin;

use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Request as RequestEntity;
use BO\Zmsentities\Scope as ScopeEntity;
use BO\Zmsentities\Schema\Entity as SchemaEntity;

use BO\Zmsentities\Collection\ProcessList;

use BO\Slim\Render;

use Psr\Http\Message\ResponseInterface;
use \Psr\Http\Message\RequestInterface;

class ConfigInfo extends BaseController
{
    /**
     * @SuppressWarnings(Param)
     * @return String
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ):ResponseInterface {
        $workstation = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 1])->getEntity();
        $config = \App::$http->readGetResult('/config/', ['withDescriptions' => 1])->getEntity();

        $mainProcessExample = ((new ProcessEntity)->getExample());
        $mainProcessExample->id = 987654;
        $dateTime = new \DateTimeImmutable("2015-10-23 08:00:00", new \DateTimeZone('Europe/Berlin'));
        $mainProcessExample->getFirstAppointment()->setDateTime($dateTime);
        $mainProcessExample->requests[] = (new RequestEntity())->getExample();

        $processExample = ((new ProcessEntity)->getExample());
        $processExample->scope = ((new ScopeEntity)->getExample());
        $processExample2 = clone $processExample;
        $dateTime = new \DateTimeImmutable("2015-12-30 11:55:00", new \DateTimeZone('Europe/Berlin'));
        $processExample2->getFirstAppointment()->setDateTime($dateTime);

        $processListExample = new ProcessList();
        $processListExample->addEntity($processExample);
        $processListExample->addEntity($processExample2);
        $success = $request->getAttribute('validator')->getParameter('success')->isString()->getValue();

        if ($request->getMethod() === 'POST') {
            $input = $request->getParsedBody();
            $entity = new SchemaEntity($input);
            $config = \App::$http->readPostResult(
                '/config/',
                $entity
            )->getEntity();
            return Render::redirect(
                'configinfo',
                [],
                [
                    'success' => 'config_saved'
                ]
            );
        }

        return Render::withHtml(
            $response,
            'page/configinfo.twig',
            [
                'title' => 'Konfiguration System',
                'workstation' => $workstation,
                'config' => $config,
                'processExample' => $mainProcessExample,
                'processListExample' => $processListExample,
                'menuActive' => 'configinfo',
                'success' => $success
            ]
        );
    }
}
