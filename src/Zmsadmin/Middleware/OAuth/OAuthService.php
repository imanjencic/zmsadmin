<?php

namespace BO\Zmsadmin\Middleware\OAuth;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Server\RequestHandlerInterface;

use BO\Zmsclient\Auth;

class OAuthService
{
    /**
     * request variable
     *
     * @var ServerRequestInterface|null
     */
    protected $request = null;

    /**
     * providerList variable
     *
     * @var array
     */
    protected $providerList = [];

    /**
     * Set the provider data from json config
     *
     * @return void
     */
    public function __construct()
    {
        $this->providerList = $this->readProviderListFromJson();
    }

    /**
     * Set the authorizsationType attribute to request and init authorization method
     *
     * @param ServerRequestInterface $request PSR7 request
     * @param RequestHandlerInterface $next Next middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(
        ServerRequestInterface $request,
        RequestHandlerInterface $next
    ): ResponseInterface {
        $request = $this->setProviderlistToRequest($request);
        $request = $this->setSelectedOauthProvider($request);
        $response = $next->handle($request);
        return $response;
    }

    public function setProviderlistToRequest(ServerRequestInterface $request)
    {
        $request = $request->withAttribute('oauthProviderList', $this->providerList);
        return $request;
    }

    public function setSelectedOauthProvider(ServerRequestInterface $request)
    {
        $selectedProvider = $request->getParam('provider') ? $request->getParam('provider') : Auth::getOidcProvider();
        if ($selectedProvider && array_key_exists($selectedProvider, $this->providerList)) {
            $request = $request->withAttribute('oauthProvider', $this->providerList[$selectedProvider]);
        }
        return $request;
    }

    public function readProviderListFromJson()
    {
        $providerList = [];
        $config_data = file_get_contents(\App::APP_PATH . '/' . \App::$oidc_provider_config);
        if (gettype($config_data) === 'string') {
            $providerDataList = json_decode($config_data, true);
        }
        foreach ($providerDataList as $providerName => $providerOptions) {
            $providerOptions['providerName'] = $providerName;
            $providerList[$providerName] = new OAuthProvider($providerOptions);
        }
        return $providerList;
    }
}
