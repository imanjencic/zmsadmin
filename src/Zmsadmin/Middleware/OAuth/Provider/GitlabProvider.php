<?php

namespace BO\Zmsadmin\Middleware\OAuth\Provider;

use BO\Zmsclient\Psr7\ClientInterface as HttpClientInterface;
use BO\Zmsclient\Psr7\Client;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use League\OAuth2\Client\Provider\GenericResourceOwner;

use Fig\Http\Message\StatusCodeInterface;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * @SuppressWarnings(unused)
 *
 */
class GitlabProvider extends AbstractProvider
{
    use BearerAuthorizationTrait;

    /**
     * Authentification options
     *
     * @var array
     */
    protected $options = [];

    /**
     * Sets the config options for gitlab access from json file.
     *
     * @param array $options An array of options to set on this provider.
     *     Options include `clientId`, `clientSecret`, `redirectUri`, `authServerurl` and `realm`.
     *     Individual providers may introduce more options, as needed.
     * @return parent
     */
    public function __construct(array $options, $client = null)
    {
        $this->options = $options;
        $client = ((null === $client)) ? new Client() : $client;
        return parent::__construct($options, ['httpClient' => $client]);
    }

    /**
     * Sets the HTTP client instance.
     *
     * @param  HttpClientInterface $client
     * @return self
     */
    public function setHttpClient($client)
    {
        $this->httpClient = $client;
        return $this;
    }

    /**
     * Sends a request instance and returns a response instance.
     *
     * WARNING: This method does not attempt to catch exceptions caused by HTTP
     * errors! It is recommended to wrap this method in a try/catch block.
     *
     * @param  RequestInterface $request
     * @return ResponseInterface
     */
    public function getResponse(RequestInterface $request)
    {
        return $this->getHttpClient()->readResponse($request);
    }

    /**
     * Creates base url from provider configuration.
     *
     * @return string
     */
    protected function getBaseUrl()
    {
        return $this->options['authServerUrl'];
    }

    /**
     * Get authorization url to begin OAuth flow
     *
     * @return string
     */
    public function getBaseAuthorizationUrl()
    {
        return $this->getBaseUrl() . $this->options['urlAuthorize'];
    }

    /**
     * Get access token url to retrieve token
     *
     * @param  array $params
     *
     * @return string
     */
    public function getBaseAccessTokenUrl(array $params)
    {
        return $this->getBaseUrl() . $this->options['urlAccessToken'];
    }

    /**
     * Get authorization url to begin OAuth flow
     *
     * @return string
     */
    public function getBaseLogoutUrl()
    {
        return $this->getBaseUrl() . $this->options['urlRevokation'];
    }

    /**
     * Get provider url to fetch user details
     *
     * @param  AccessToken $token
     *
     * @return string
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token)
    {
        return $this->getBaseUrl() . $this->options['urlResourceOwnerDetails'];
    }

    /**
     * Builds the revoke URL.
     *
     * @param ResponseInterface $response
     * @param AccessToken|null $token
     * @param array $options
     * @return ResponseInterface $response
     */
    public function getRevokeResponse(
        ResponseInterface $response,
        AccessToken $accessToken = null,
        array $options = []
    ) {
        $url = $this->getBaseLogoutUrl();
        $params = $this->getAuthorizationParameters($options);
        $request = $this->getAuthenticatedRequest(self::METHOD_POST, $url, $accessToken, $params);
        return ($this->getResponse($request)) ?
            $response->withRedirect('?state='. $params['state'], StatusCodeInterface::STATUS_FOUND) :
            $response;
    }

    /**
     * Get the default scopes used by this provider.
     *
     * This should not be a complete list of all scopes, but the minimum
     * required for the provider user interface!
     *
     * @return string[]
     */
    protected function getDefaultScopes()
    {
        return ['profile', 'email', 'openid'];
    }

    /**
     * Returns the string that should be used to separate scopes when building
     * the URL for requesting an access token.
     *
     * @return string Scope separator, defaults to ','
     */
    protected function getScopeSeparator()
    {
        return ' ';
    }

    /**
     * Generate a user object from a successful user details request.
     *
     * @param array $response
     * @param AccessToken $token
     * @return GenericResourceOwner
     */
    protected function createResourceOwner(array $response, AccessToken $token)
    {
        return new GenericResourceOwner($response, $token->getToken());
    }

    /**
     * Check a provider response for errors.
     *
     * @throws IdentityProviderException
     * @param  ResponseInterface $response
     * @param  string $data Parsed response data
     * @return void
     */
    protected function checkResponse(ResponseInterface $response, $data)
    {
        if (!empty($data['error'])) {
            $error = $data['error'];
            if (isset($data['error_description'])) {
                $error.=': '.$data['error_description'];
            }
            throw new IdentityProviderException($error, 0, $data);
        }
    }
}
