<?php
namespace BO\Zmsadmin\Middleware\OAuth;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

use BO\Zmsadmin\Middleware\OAuth\ResourceOwner\Keycloak as KeycloakResourceOwner;
use BO\Zmsadmin\Middleware\OAuth\ResourceOwner\Gitlab as GitlabResourceOwner;

class OAuthResourceOwner
{
    protected $grantOptions = [];

    protected $providerName = '';

    protected $ownerData = [];

    public function __construct(ResourceOwnerInterface $resourceOwner, string $providerName, array $grantOptions = [])
    {
        $this->grantOptions = $grantOptions;
        $this->providerName = $providerName;
        if ($providerName === 'keycloak') {
            $this->ownerData = $this->getOwnerDataFromKeycloak($resourceOwner);
        }
        if ($providerName === 'gitlab') {
            $this->ownerData = $this->getOwnerDataFromGitlab($resourceOwner);
        }
    }

    public function getOwnerData()
    {
        return $this->ownerData;
    }

    /**
     * Requests and returns the resource owner data of keycloak provider.
     *
     * @param  ResourceOwnerInterface $resourceOwner
     * @return Array
     */
    protected function getOwnerDataFromKeycloak(ResourceOwnerInterface $resourceOwner)
    {
        $ownerData = [];
        $owner = new KeycloakResourceOwner($resourceOwner->toArray());
        $ownerData['username'] = $owner->getName(). '@' . $this->providerName;
        $ownerData['email'] = $owner->getEmail();
        $ownerData['sub'] = $owner->getId();
        if (isset($this->grantOptions['onlyVerifiedMail']) && $this->grantOptions['onlyVerifiedMail']) {
            $ownerData['email'] = $owner->getVerifiedEmail();
        }
        return $ownerData;
    }

    /**
     * Requests and returns the resource owner data of gitlab provider.
     *
     * @param  ResourceOwnerInterface $resourceOwner
     * @return Array
     */
    protected function getOwnerDataFromGitlab(ResourceOwnerInterface $resourceOwner)
    {
        $ownerData = [];
        $owner = new GitlabResourceOwner($resourceOwner->toArray());
        $ownerData['username'] = $owner->getName(). '@' . $this->providerName;
        $ownerData['email'] = $owner->getEmail();
        $ownerData['sub'] = $owner->getId();
        if ($this->grantOptions['onlyVerifiedMail']) {
            $ownerData['email'] = $owner->getVerifiedEmail();
        }
        return $ownerData;
    }
}
