<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsadmin;

use BO\Zmsentities\Collection\MaintenanceScheduleList;
use BO\Zmsentities\Config;
use BO\Zmsentities\MaintenanceSchedule;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class MaintenanceList extends BaseController
{
    /**
     * @SuppressWarnings(UnusedFormalParameter)
     * @return ResponseInterface
     */
    public function readResponse(
        RequestInterface $request,
        ResponseInterface $response,
        array $args
    ): ResponseInterface {
        /** @var Config $config */
        $workstation  = \App::$http->readGetResult('/workstation/', ['resolveReferences' => 0])->getEntity();
        $config       = \App::$http->readGetResult('/config/')->getEntity();
        $scheduleList = \App::$http->readGetResult('/maintenanceschedule/')->getCollection();
        $scheduleList = $scheduleList ?? new MaintenanceScheduleList();
        $scheduleList->sortByScheduleTime();

        $configEntry = $config->getEntryOrDefault(Config::PROPERTY_MAINTENANCE_NEXT);
        $inSchedule = null;
        if (!empty($configEntry)) {
            $inSchedule = new MaintenanceSchedule(json_decode($configEntry, true));
        }

        foreach ($scheduleList as $key => $schedule) {
            $scheduleList[$key]['nextRun'] = null;
            if ($schedule['isRepetitive'] === true && $inSchedule
            && $inSchedule->getId() === $schedule->getId()) {
                $scheduleList[$key]['nextRun'] = $inSchedule->getStartDateTime()->format('Y-m-d H:i:s');
            } elseif (!$schedule['isRepetitive']) {
                $scheduleList[$key]['nextRun'] = 'never';
                if ((new \DateTime($schedule['timeString'])) > (new \DateTime())) {
                    $scheduleList[$key]['nextRun'] = $schedule['timeString'];
                }
            }

            if ($schedule['isActive'] === true) {
                $scheduleList[$key]['nextRun'] = $scheduleList[$key]['startDateTime']->format('Y-m-d H:i:s');
            }
        }

        return \BO\Slim\Render::withHtml(
            $response,
            'page/maintenanceList.twig',
            [
                'title' => 'Wartungsmodus Terminplan',
                'menuActive' => 'maintenance',
                'workstation' => $workstation,
                'scheduledMaintenance' => $scheduleList,
                'reloadInterval' => 5000,
                'success' => $request->getAttribute('validator')->getParameter('success')->isString()->getValue(),
            ]
        );
    }
}
