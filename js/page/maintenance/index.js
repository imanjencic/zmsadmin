import BaseView from '../../lib/baseview'
import $ from 'jquery'


class View extends BaseView {

    constructor(element, options) {
        super(element);
        this.$main = $(element);
        this.reloadTimer;
        this.reloadInterval = options.reloadinterval | 5000;
        this.includeUrl = options.includeurl;
        this.bindPublicMethods(
            'bindEvents'
        );
        $(() => {
            this.initRepetiveTimeView();
            this.fetchStatsList();
            this.setReloadTimer();
            this.bindEvents();
        });
    }

    bindEvents() { 
        this.$main.off('click').on('change', '#scheduleRepetition', (event) => {
            this.onChangeRepitition(event);
        }).on('click', '#btn_announcement_edit', () => {
            $('#announcement_preview').hide();
            $('#announcement_edit').show();
            return false;
        }).on('click', '#btn_documentBody_edit', () => {
            $('#documentBody_preview').hide();
            $('#documentBody_edit').show();
            return false;
        });
    }

    initRepetiveTimeView() {
        if ($('#form_group_repetitiveTime').length) {
            $('#form_group_repetitiveTime').hide();
            $('#repetitiveTime').attr("name", "");
        }
    }

    onChangeRepitition(event) {
        if (event.target.checked) {
            $('#form_group_repetitiveTime').show();
            $('#form_group_singleTime').hide();
            $('#repetitiveTime').attr("name", "timeString");
            $('#singleTime').attr("name", "");
        } else {
            $('#form_group_repetitiveTime').hide();
            $('#form_group_singleTime').show();
            $('#repetitiveTime').attr("name", "");
            $('#singleTime').attr("name", "timeString");
        }
    }

    fetchStatsList()
    {
        const url = `${this.includeUrl}/accessstats/`;
        fetch(url)
            .then((response) => response.json())
            .then((data) => { 
                this.processStatsList(data);
            });
    }

    processStatsList(data)
    {
        const occurrences = data.reduce(function (acc, item) {
            return acc[item.role] ? ++acc[item.role] : acc[item.role] = 1, acc
          }, {});
          

        $('#activeCitizen').text(occurrences['citizen']|0);
        $('#activeUsers').text(occurrences['user']|0);
    }

    setReloadTimer() {
        clearTimeout(this.reloadTimer);
        this.reloadTimer = setTimeout(() => {
            this.fetchStatsList();
            this.setReloadTimer();
        }, this.reloadInterval);
    }
}

export default View;
